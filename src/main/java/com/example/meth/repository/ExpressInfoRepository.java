package com.example.meth.repository;

import com.example.meth.model.oracle.ExpressInfo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ExpressInfoRepository extends JpaRepository<ExpressInfo, Long> {
}
