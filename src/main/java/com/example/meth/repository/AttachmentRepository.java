package com.example.meth.repository;

import com.example.meth.model.postgre.Attachment;
import com.example.meth.model.postgre.Client;
import com.example.meth.model.postgre.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface AttachmentRepository extends JpaRepository<Attachment, Long> {
}
