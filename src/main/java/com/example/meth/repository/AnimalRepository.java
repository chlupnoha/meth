package com.example.meth.repository;

import com.example.meth.model.postgre.Address;
import com.example.meth.model.postgre.AnimalEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import javax.persistence.ColumnResult;
import javax.persistence.EntityResult;
import javax.persistence.FieldResult;
import javax.persistence.SqlResultSetMapping;

@Repository
public interface AnimalRepository extends JpaRepository<AnimalEntity, Long> {

    @Query(
            value = "SELECT animal.id AS id, COALESCE(animal.amount,0) AS amount, UPPER(animal.name) AS name FROM animal animal WHERE animal.name = :name",
            nativeQuery = true)
    Page<AnimalEntity> findAllLikeNameAndOwnerSorted(String name, Pageable pageable);


}
