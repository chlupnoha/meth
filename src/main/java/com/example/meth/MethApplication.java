package com.example.meth;

import com.example.meth.model.postgre.Client;
import com.example.meth.model.postgre.ERole;
import com.example.meth.model.postgre.Role;
import com.example.meth.repository.ClientRepository;
import com.example.meth.repository.RoleRepository;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

@SpringBootApplication
public class MethApplication {

	public static void main(String[] args) {
		SpringApplication.run(MethApplication.class, args);
	}

//	@Autowired
//	private ClientRepository clientRepository;
//
//	@Autowired
//	RoleRepository roleRepository;
//
//	@EventListener(ApplicationReadyEvent.class)
//	public void runAfterStartup() throws IOException {
//		File file = new File("/home/jordan/Pictures/editor.png");
//		FileInputStream fis = new FileInputStream(file);
//		byte[] bytes = IOUtils.toByteArray(fis);
//		Client client = new Client("client", "email", bytes);
//		clientRepository.save(client);
//		System.out.println(client.getId());
//		clientRepository.findAll().forEach(System.out::println);
//
//		List<Role> roles = roleRepository.findAll();
//		if(roles.size() == 0){
//			Arrays.stream(ERole.values()).forEach(x -> {
//				roleRepository.save(new Role(x));
//			});
//		}else{
//			roles.forEach(System.out::println);
//		}
//	}

	//AUTHORIZATION
//	https://www.bezkoder.com/spring-boot-react-jwt-auth/
//	https://www.bezkoder.com/spring-boot-security-postgresql-jwt-authentication/
//	https://www.bezkoder.com/react-jwt-auth/

//	https://jasonwatmore.com/post/2018/09/11/react-basic-http-authentication-tutorial-example

}