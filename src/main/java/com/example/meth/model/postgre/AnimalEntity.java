package com.example.meth.model.postgre;

import javax.persistence.*;

@Entity
@Table(name = "animal")
public class AnimalEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	private String name;

	private Integer amount;

	public AnimalEntity() {
	}

	public AnimalEntity(Integer amount, String name) {
		this.amount = amount;
		this.name = name;
	}

	public Long getId() {
		return id;
	}

	public Integer getAmount() {
		return amount;
	}

	public void setAmount(Integer amount) {
		this.amount = amount;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String toString() {
		return "AnimalEntity{" +
				"id=" + id +
				", amount=" + amount +
				", name='" + name + '\'' +
				'}';
	}
}