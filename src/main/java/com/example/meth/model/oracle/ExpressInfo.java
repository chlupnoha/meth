package com.example.meth.model.oracle;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "e")
public class ExpressInfo {

    @Id
    @GeneratedValue
    private Long id;

    private String expressInfo;

    public ExpressInfo() {
    }

    public ExpressInfo(String expressInfo) {
        this.expressInfo = expressInfo;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getExpressInfo() {
        return expressInfo;
    }

    public void setExpressInfo(String expressInfo) {
        this.expressInfo = expressInfo;
    }
}
