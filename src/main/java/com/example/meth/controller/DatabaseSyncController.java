package com.example.meth.controller;

import com.example.meth.model.postgre.Client;
import com.example.meth.model.oracle.ExpressInfo;
import com.example.meth.repository.ExpressInfoRepository;
import com.example.meth.repository.ClientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/database-sync")
public class DatabaseSyncController {

    @Autowired
    private ClientRepository clientRepository;

    @Autowired
    private ExpressInfoRepository clientExpressRepository;

    private void zkouska() {
        Client client = new Client("client", "email");
        clientRepository.save(client);
        System.out.println(client.getId());
        clientRepository.findAll().forEach(System.out::println);

        ExpressInfo express = new ExpressInfo("expressInfo");
        clientExpressRepository.save(express);
        System.out.println(express.getId());
        clientExpressRepository.findAll().forEach(System.out::println);
    }

    @GetMapping
    public List<Client> getClients() {
        zkouska();

        return clientRepository.findAll();
    }
//
//    @GetMapping("/{id}")
//    public Client getClient(@PathVariable Long id) {
//        return clientRepository.findById(id).orElseThrow(RuntimeException::new);
//    }
//
//    @PostMapping
//    public ResponseEntity createClient(@RequestBody Client client) throws URISyntaxException {
//        Client savedClient = clientRepository.save(client);
//        return ResponseEntity.created(new URI("/clients/" + savedClient.getId())).body(savedClient);
//    }
//
//    @PutMapping("/{id}")
//    public ResponseEntity updateClient(@PathVariable Long id, @RequestBody Client client) {
//        Client currentClient = clientRepository.findById(id).orElseThrow(RuntimeException::new);
//        currentClient.setName(client.getName());
//        currentClient.setEmail(client.getEmail());
//        currentClient = clientRepository.save(client);
//
//        return ResponseEntity.ok(currentClient);
//    }
//
//    @DeleteMapping("/{id}")
//    public ResponseEntity deleteClient(@PathVariable Long id) {
//        clientRepository.deleteById(id);
//        return ResponseEntity.ok().build();
//    }

}
