package com.example.meth.controller;

import com.example.meth.model.postgre.Attachment;
import com.example.meth.model.postgre.Client;
import com.example.meth.payload.response.EnrichedClient;
import com.example.meth.repository.AttachmentRepository;
import com.example.meth.repository.ClientRepository;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

//TODO tady pak zabezpeceni podle pozadavku
@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/clients")
public class ClientsController {

    @Autowired
    public ClientRepository clientRepository;

    @Autowired
    public AttachmentRepository attachmentRepository;

    private List<EnrichedClient> enrichedClientList(){
        return clientRepository.findAll().stream().map(EnrichedClient::new).collect(Collectors.toList());
    }

    @GetMapping
    public List<EnrichedClient> getClients() {
        return clientRepository.findAll().stream().map(EnrichedClient::new).collect(Collectors.toList());
    }

    @GetMapping("/{id}")
    public Client getClient(@PathVariable Long id) {
        return clientRepository.findById(id).orElseThrow(RuntimeException::new);
    }

    @PostMapping
    public ResponseEntity createClient(@RequestBody Client client) throws URISyntaxException {
        Client savedClient = clientRepository.save(client);
        return ResponseEntity.created(new URI("/clients/" + savedClient.getId())).body(savedClient);
    }

    @PostMapping("/dummy-create")
    @ResponseStatus(value = HttpStatus.NO_CONTENT)
    public void createDummyClients() throws URISyntaxException {
        int size = clientRepository.findAll().size();
        for (int i = 0; i < 3; i++) {
            clientRepository.save(new Client("client n." + (size+i), "email" + (size+i) + "@meth.com"));
        }
    }

    @PutMapping("/{id}")
    public ResponseEntity updateClient(@PathVariable Long id, @RequestBody Client client) {
        Client currentClient = clientRepository.findById(id).orElseThrow(RuntimeException::new);
        currentClient.setName(client.getName());
        currentClient.setEmail(client.getEmail());
        currentClient = clientRepository.save(client);

        return ResponseEntity.ok(currentClient);
    }

    //todo move to separate controller?

    @GetMapping("/{id}/get-attachments")
    public Set<Attachment> getClientAttachments(@PathVariable Long id) {
        System.out.println("Client id: " + id);
        Set<Attachment> attachments = clientRepository.findById(id).orElseThrow(RuntimeException::new).getAttachments();
        System.out.println("Attachment l: " + attachments.size());
        return attachments;
    }

    @PostMapping("/{id}/upload-file")
    public ResponseEntity uploadFile(@PathVariable Long id, @RequestParam("File[]") List<MultipartFile> files) throws IOException {
        Client currentClient = clientRepository.findById(id).orElseThrow(RuntimeException::new);
        Set<Attachment> attachments = currentClient.getAttachments();

        for(MultipartFile file : files){
            InputStream inputStream = file.getInputStream();
            Attachment attachment = new Attachment(file.getOriginalFilename(), IOUtils.toByteArray(inputStream));
            attachmentRepository.save(attachment);
            attachments.add(attachment);
        }
        currentClient.setAttachments(attachments);
        clientRepository.save(currentClient);

        return ResponseEntity.ok(enrichedClientList());
    }

    @DeleteMapping("/{id}/delete-file")
    public ResponseEntity deleteFile(@PathVariable Long id) {
        attachmentRepository.deleteById(id);
        return ResponseEntity.ok().build();
    }

    @DeleteMapping("/{id}")
    public ResponseEntity deleteClient(@PathVariable Long id) {
        clientRepository.deleteById(id);
        return ResponseEntity.ok().build();
    }

    @GetMapping(value = "/{attachmentId}/attachment", produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
    public ResponseEntity<byte[]> getImage(@PathVariable Long attachmentId) throws IOException {
        //TODO nejaky zabezpeceni
        Attachment attachment = attachmentRepository.findById(attachmentId).orElseThrow(RuntimeException::new);
        byte[] file = attachment.getFile();
        HttpHeaders responseHeaders = new HttpHeaders();
        responseHeaders.setContentType(MediaType.APPLICATION_OCTET_STREAM);
        responseHeaders.set("Content-Disposition", "attachment; filename=" + attachment.getName());

        return new ResponseEntity<>(file, responseHeaders, HttpStatus.OK);
    }

}
