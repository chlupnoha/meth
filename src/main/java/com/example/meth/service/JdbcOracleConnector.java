package com.example.meth.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.*;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

//Nakonfigurovat to ma jit nejak takto: https://www.baeldung.com/spring-boot-configure-multiple-datasources

@Component
public class JdbcOracleConnector {

  private Connection oracleConnection;

  @Value("${spring.datasource.oracle.jdbc-url}")
  private String jdbcUrl;

  @Value("${spring.datasource.oracle.username}")
  private String username;

  @Value("${spring.datasource.oracle.password}")
  private String password;

  @Value("${spring.datasource.oracle.driverClassName}")
  private String driverClassName;


  synchronized public Connection getConnection() {
    if(oracleConnection == null){
      try {
        // registers Oracle JDBC driver - though this is no longer required
        // since JDBC 4.0, but added here for backward compatibility
        Class.forName(driverClassName);

        Properties connectionProperties = new Properties();
        connectionProperties.put("user", username);
        connectionProperties.put("password", password);
        oracleConnection = DriverManager.getConnection(jdbcUrl, connectionProperties);
        if (oracleConnection != null) {
          System.out.println("Connected with connection #1");
        }
//      //step3 create the statement object
//      Statement stmt=oracleConnection.createStatement();
//
//      //step4 execute query
//      ResultSet rs=stmt.executeQuery("select * from emp");
//      while(rs.next())
//        System.out.println(rs.getInt(1)+"  "+rs.getString(2)+"  "+rs.getString(3));

        return oracleConnection;
      } catch (ClassNotFoundException | SQLException ex) {
        ex.printStackTrace();
      } finally {
        try {
          if (oracleConnection != null && !oracleConnection.isClosed()) {
            oracleConnection.close();
          }
        } catch (SQLException ex) {
          ex.printStackTrace();
        }
      }
    }else{
      return oracleConnection;
    }

    return oracleConnection;
  }

}
