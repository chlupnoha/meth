package com.example.meth.payload.response;

import com.example.meth.model.postgre.Client;

public class EnrichedClient {
	private Long id;

	private String name;

	private String email;

	private Long attachmentCount;

	public EnrichedClient() {

	}

	public EnrichedClient(Long id, String name, String email, Long attachmentCount) {
		this.id = id;
		this.name = name;
		this.email = email;
		this.attachmentCount = attachmentCount;
	}

	public EnrichedClient(Client c){
		this.id = c.getId();
		this.name = c.getName();
		this.email = c.getEmail();
		this.attachmentCount = (long) c.getAttachments().size();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Long getAttachmentCount() {
		return attachmentCount;
	}

	public void setAttachmentCount(Long attachmentCount) {
		this.attachmentCount = attachmentCount;
	}
}
