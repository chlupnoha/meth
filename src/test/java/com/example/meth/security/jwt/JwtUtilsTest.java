package com.example.meth.security.jwt;

import com.example.meth.security.services.UserDetailsImpl;
import io.jsonwebtoken.ExpiredJwtException;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class JwtUtilsTest {

    @Autowired
    JwtUtils jwtUtils;

    @Test
    void generateJwtToken() {
        UserDetailsImpl userDetails = new UserDetailsImpl(1L, "u", "e", "p", null);
        Authentication authentication = Mockito.mock(Authentication.class);
        Mockito.when(authentication.getPrincipal()).thenReturn(userDetails);

        String token = jwtUtils.generateJwtToken(authentication);

        assertEquals(168, token.length());
    }

    @Test()
    void getUserNameFromJwtToken() {
        String token = "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJ1IiwiaWF0IjoxNjQ5MzM1ODE1LCJleHAiOjE2NDk0MjIyMTV9.paDnVhF_FlELcJmgjU1dl4YzeCUIfIkP4aRIdPBji0r--YfHppyXe7NECaz-f4kibZ89bqogREow38ISqdWSYQ";

        assertThrows(ExpiredJwtException.class, () -> jwtUtils.getUserNameFromJwtToken(token));
    }

    @Test
    void validateJwtToken() {
        //TODO handle
        String token = "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJ1IiwiaWF0IjoxNjQ5MzM1ODE1LCJleHAiOjE2NDk0MjIyMTV9.paDnVhF_FlELcJmgjU1dl4YzeCUIfIkP4aRIdPBji0r--YfHppyXe7NECaz-f4kibZ89bqogREow38ISqdWSYQ";

        boolean validateJwtToken = jwtUtils.validateJwtToken(token);

        assertFalse(validateJwtToken);
    }
}