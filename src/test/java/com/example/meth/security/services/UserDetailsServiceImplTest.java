package com.example.meth.security.services;

import com.example.meth.model.postgre.User;
import com.example.meth.repository.UserRepository;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class UserDetailsServiceImplTest {

    @Autowired
    UserRepository userRepository;

    @Autowired
    UserDetailsServiceImpl userDetailsService;

    @BeforeEach
    void init() {
        userRepository.save(new User("karel", "email", "pass"));
    }

    @Test
    void loadUserByUsername() {
        UserDetails user = userDetailsService.loadUserByUsername("karel");

        assertEquals(user.getUsername(), "karel");
    }

    @Test
    void loadUserByUsernameNotFound() {
        Throwable exception = assertThrows(UsernameNotFoundException.class, () -> userDetailsService.loadUserByUsername("notFound"));
        assertEquals("User Not Found with username: notFound", exception.getMessage());
    }

    @AfterEach
    void teardown() {
        userRepository.deleteAll();
    }
}