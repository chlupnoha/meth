package com.example.meth.security.services;

import com.example.meth.model.postgre.ERole;
import com.example.meth.model.postgre.Role;
import com.example.meth.model.postgre.User;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

class UserDetailsImplTest {

    @Test
    void build() {
        Role role = new Role(ERole.ROLE_USER);
        User user = new User("username", "email", "password");
        user.setId(1L);
        user.setRoles(new HashSet<>(Collections.singletonList(role)));

        UserDetailsImpl userDetail = UserDetailsImpl.build(user);

        List<String> expected = Arrays.asList("ROLE_USER");
        List<String> res = userDetail.getAuthorities().stream().map(Object::toString).collect(Collectors.toList());
        assertEquals(expected, res);
    }

}