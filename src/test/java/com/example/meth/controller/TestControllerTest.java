package com.example.meth.controller;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

import static org.hamcrest.Matchers.containsString;
import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
class TestControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    void allAccess() throws Exception {
        this.mockMvc.perform(get("/api/test/all"))
                .andDo(print()).andExpect(status().isOk())
                .andExpect(content().string(containsString("Public Content.")));
    }

    @Test
    void userAccessUnauthorized() throws Exception {
        this.mockMvc.perform(get("/api/test/user"))
                .andDo(print()).andExpect(status().isUnauthorized());
    }

    @Test
    @WithMockUser(roles = "USER")
    void userAccess() throws Exception {
        this.mockMvc.perform(get("/api/test/user"))
                .andDo(print()).andExpect(status().isOk())
                .andExpect(content().string(containsString("User Content.")));
    }

//    @Test
//    void moderatorAccess() {
//    }
//
//    @Test
//    void adminAccess() {
//    }
}