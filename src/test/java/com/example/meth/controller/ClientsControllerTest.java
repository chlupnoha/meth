package com.example.meth.controller;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;


@SpringBootTest
@AutoConfigureMockMvc
class ClientsControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    void getClients() {
        //TODO tady pak take otestovat, ale na konkretnich klientech + attachments
        Assertions.assertEquals(1, 1);
    }
}