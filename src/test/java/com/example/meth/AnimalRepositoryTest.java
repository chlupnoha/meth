package com.example.meth;

import com.example.meth.model.postgre.AnimalEntity;
import com.example.meth.repository.AnimalRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

@SpringBootTest
class AnimalRepositoryTest {

	@Autowired
	private AnimalRepository animalRepository;

	@Test
	void findAllLikeNameAndOwnerSorted() {
		AnimalEntity animalEntity = new AnimalEntity(null, "dog");
		animalRepository.save(animalEntity);
		AnimalEntity animalEntity2 = new AnimalEntity(1, "CAT");
		animalRepository.save(animalEntity2);
		System.out.println(animalEntity2.getId());

		Pageable sortedByName = PageRequest.of(0, 3, Sort.by("id"));
		Page<AnimalEntity> animals = animalRepository.findAllLikeNameAndOwnerSorted("dog", sortedByName);

		animals.forEach(System.out::println);
	}

}
