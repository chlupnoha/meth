package com.example.meth;

import com.example.meth.model.postgre.Address;
import com.example.meth.model.postgre.Client;
import com.example.meth.model.oracle.ExpressInfo;
import com.example.meth.repository.AddressRepository;
import com.example.meth.repository.ExpressInfoRepository;
import com.example.meth.repository.ClientRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.ActiveProfiles;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;

@ActiveProfiles("multipledatasources")
@DataJpaTest // no test database!
class MultipleDatasourcesIntegrationTest {

    @Autowired
    ClientRepository clientRepo;
    @Autowired
    AddressRepository addressRepository;
    @Autowired
    ExpressInfoRepository clientExpressRepository;

    @Test
    void shouldSaveClient() {
        Client client = new Client("name", "email");

        Client saved = clientRepo.save(client);

        Optional<Client> result= clientRepo.findById(saved.getId());
        assertThat(result).isPresent();
    }

    @Test
    void shouldSaveAddress() {
        Address address = new Address("addr");

        Address saved = addressRepository.save(address);

        Optional<Address> result= addressRepository.findById(saved.getId());
        assertThat(result).isPresent();
    }

    @Test
    void shouldSaveClientExpress() {
        ExpressInfo clientExpress = new ExpressInfo("expressInfo");

        ExpressInfo saved = clientExpressRepository.save(clientExpress);

        Optional<ExpressInfo> result= clientExpressRepository.findById(saved.getId());
        assertThat(result).isPresent();
    }

}
