#FROM openjdk:8
#ARG JAR_FILE=./*.jar
#COPY Meth-0.0.1-SNAPSHOT.jar application.jar
#ENTRYPOINT ["java", "-jar", "application.jar"]

FROM maven:3.6.0-jdk-8-slim AS build
COPY src /home/app/src
COPY frontend /home/app/frontend
COPY pom.xml /home/app
RUN mvn -f /home/app/pom.xml clean package

FROM openjdk:8-jre-slim
COPY --from=build /home/app/target/Meth-0.0.1-SNAPSHOT.jar /usr/local/lib/application.jar
EXPOSE 8080
ENTRYPOINT ["java","-jar","/usr/local/lib/application.jar"]