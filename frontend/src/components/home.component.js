import React, { useState, useCallback, useEffect } from 'react'
import { Modal } from 'react-bootstrap';
import { useDropzone } from 'react-dropzone';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import filter from '@inovua/reactdatagrid-community/filter'
import "./../App.css";

import ClientService from "../services/client.service";
import ReactDataGrid from '@inovua/reactdatagrid-community';
import '@inovua/reactdatagrid-community/index.css';
import Button from '@inovua/reactdatagrid-community/packages/Button'

//https://reactjs.org/docs/hooks-state.html
//https://reactdatagrid.io/docs/getting-started#using-async-or-remote-data

const gridStyle = { minHeight: 600, marginTop: 10 };

const Home = () => {

const defaultFilterValue = [
   { name: 'id', operator: 'startsWith', type: 'string', value: '' },
   { name: 'name', operator: 'startsWith', type: 'string', value: '' },
   { name: 'email', operator: 'startsWith', type: 'string', value: '' }
];

  useEffect(() => {
    console.log("I have been mounted")
    ClientService.dummyCreate();
    ClientService.getClients().then(response => {
      setDataSource(response.data);
    });
  }, [])

  const [show, setShow] = useState(false);
  const handleClose = () => setShow(false);

  const [showAttachment, setShowAttachment] = useState(false);

  const [selectedRec, setSelectedRec] = useState(0);
  const handleSelectedRec = (recId) => setSelectedRec(recId);

  const [attachments, setAttachments] = useState([]);
  const handleSetAttachments = (att) => setAttachments(att);

  const defaultColumns = [
    { name: 'id', header: 'ID', minWidth: 50, defaultFlex: 1 },
    { name: 'name', header: 'Name', minWidth: 50, defaultFlex: 1 },
    { name: 'email', header: 'Email', minWidth: 50, defaultFlex: 1 },
    { name: 'attachmentCount', header: 'Attachments Count', minWidth: 50, defaultFlex: 1 },
    {
      name: 'actions', header: 'Attachments', minWidth: 50, maxWidth: 140, defaultFlex: 1,

      render: ({ value, data }) => {
        //tohle je trosku divocina, ale nevim, jak se to presne resi :/
        const addHandler = (event) => {
          setShowAttachment(false)
          setShow(true);
          handleSelectedRec(data.id)
        };
        const showHandler = () => {
          setShowAttachment(true)
          setShow(true);
          handleSelectedRec(data.id)
          ClientService.getClientAttachements(data.id).then(res => {
            console.log(res.data)
            handleSetAttachments(res.data)
          });
        };

        return <div style={{ display: 'inline-block' }}>
          <Button onClick={addHandler}>Add</Button>
          <Button onClick={showHandler}>Show</Button>
        </div>
      }
    },
  ];

  const [dataSource, setDataSource] = useState([]);
  const [columns] = useState(defaultColumns);
  const [filterValue, setFilterValue] = useState(defaultFilterValue);

  const loadData = useCallback(() => {
    ClientService.getClients().then(response => {
      setDataSource(response.data);
    });
    // const newDataSource = () => {
    //   return fetch("http://localhost:8080/api/clients/")
    //     .then(response => {
    //       const totalCount = response.headers.get('X-Total-Count');
    //       return response.json().then(data => {
    //         return { data, count: totalCount * 1 };
    //       })
    //     });
    // }

    // setDataSource(newDataSource)
  }, []);

  const onFilterValueChange = useCallback((filterValue) => {
    ClientService.getClients().then(response => {
      console.log(filterValue)
      console.log(response.data)
      const data = filter(response.data, filterValue)
      console.log(data)

      setFilterValue(filterValue);
      setDataSource(data);
    });
  }, [])

  const { acceptedFiles, getRootProps, getInputProps, inputRef } = useDropzone();

  const handleFileSubmit = () => {
    if (acceptedFiles.length > 0) {
      let fd = new FormData();
      acceptedFiles.map((file) => {
        fd.append('File[]', file);
      });

      const newDataSource = () => {
        return ClientService.uploadFile(selectedRec, fd).then(res => {
          setShow(false);
          acceptedFiles.length = 0
          acceptedFiles.splice(0, acceptedFiles.length)
          inputRef.current.value = ''
          
          return { data: res.data }
        });
      }

      setDataSource(newDataSource)
    } else {
      setShow(false);
    }

  }
    ;
  const files = acceptedFiles.map(file => (
    <li key={file.path}>
      {file.path} - {file.size} bytes
    </li>
  ));

  const atta = attachments.map(a => (
    <li key={a.id}>
      id: {a.id}; Name: {a.name};  <a href={ClientService.getAttachmentLink(a.id)} download>Download</a>
    </li>
  ));

  return (
    <div>
      {/* <Button variant="primary" onClick={handleShow}>
        Launch demo modal {selectedRec}
      </Button> */}

      <Button onClick={() => loadData()} style={{ marginRight: 10 }}>
        Load async data
      </Button>
      <Button
        disabled={Array.isArray(dataSource)}
        onClick={() => setDataSource([])}
      >Clear data</Button>
      
      <ReactDataGrid
        style={gridStyle}
        idProperty="id"
        columns={columns}
        dataSource={dataSource}
        filterValue={filterValue}
        pagination="local"
        defaultLimit={30}
        onFilterValueChange={onFilterValueChange}
      />

      <Modal show={show} onHide={handleClose} size="lg">
        <Modal.Header closeButton>
          <Modal.Title>Record n. {selectedRec}</Modal.Title>
        </Modal.Header>

        <Modal.Body>
          {!showAttachment &&
            <section className="container">
              <div {...getRootProps({ className: 'dropzone' })}>
                <input {...getInputProps()} />
                <p>Drag 'n' drop some files here, or click to select files</p>
              </div>
              <hr></hr>
              <aside>
                <h4>Files:</h4>
                <ul>{files}</ul>
              </aside>
              <hr></hr>
            </section>
          }
          {showAttachment &&
            <section className="container">
              {atta}
            </section>
          }
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleClose}>
            Close
          </Button>
          <Button variant="primary" onClick={handleFileSubmit}>
            Submit files
          </Button>
        </Modal.Footer>
      </Modal>

      <ToastContainer />
    </div>
  )
}

export default () => <Home />


//var image = null
//var karel = null
//var isSelected = false
////id, name, email, attachments
//const columns = [
//  { name: 'id', header: 'ID', minWidth: 50, defaultFlex: 1 },
//  { name: 'name', header: 'Name', minWidth: 50, defaultFlex: 1 },
//  { name: 'email', header: 'Email', minWidth: 50, defaultFlex: 1},
//  { name: 'actions', header: 'Attachments', minWidth: 280, defaultFlex: 1,
//        headerProps: {
//            style: { minHeight: '38px' }
//        },
//        render: ({ value, data }) => {
//        const changeHandler = (event) => {
//            console.log(event.target.files[0]);
//            image = event.target.files[0];
//            isSelected = true;
//        };
//        const submitHandler = () => {
//            console.log(image);
//            //TODO tady poslat
//            image = null;
//            karel = true;
//        };
//
//          return <div style={{ display: 'inline-block' }}>
//			<input type="file" name="Upload" onChange={changeHandler} />
//            <Button onClick={submitHandler}>Submit</Button>
//          </div>
//        }
//  },
//];
//
//const gridStyle = { minHeight: 550 };
//
//
//const filterValue = [
//  { name: 'id', operator: 'eq', type: 'number' },
//  { name: 'name', operator: 'startsWith', type: 'string', value: '' },
//  { name: 'email', operator: 'startsWith', type: 'string', value: '' }
//];
//
//export default class Home extends Component {
//  constructor(props) {
//    super(props);
//
//    this.state = {
//      content: "",
//      dataSource: []
//    };
//    this.generateDummy = this.generateDummy.bind(this);
//  }
//
//  componentDidMount() {
//    UserService.getPublicContent().then(
//      response => {
//        this.setState({
//          content: response.data
//        });
//      },
//      error => {
//        this.setState({
//          content:
//            (error.response && error.response.data) || error.message || error.toString()
//        });
//      }
//    );
//  }
//
//  generateDummy() {
//    ClientService.dummyCreate().then(
//        response => {
//            ClientService.getClients().then(
//                response => {
//                    this.setState({
//                      dataSource: response.data
//                    });
//                }
//            )
//        }
//    );
//  }
//
//  render() {
//    return (
//        <div>
//          <div className="container">
//            <header className="jumbotron">
//              <Button onClick={this.generateDummy}>ADD DATA</Button>
//              <h3>Content: {this.state.content}</h3>
//                <ReactDataGrid
//                  idProperty="id"
//                  columns={columns}
//                  dataSource={this.state.dataSource}
//                  style={gridStyle}
//                  defaultFilterValue={filterValue}
//                />
//            </header>
//          </div>
//
//        </div>
//    );
//  }
//}
