import axios from 'axios';
import authHeader from './auth-header';

const API_URL = 'http://localhost:8080/api/clients/';

class ClientService {

  getClients() {
    return axios.get(API_URL);
  }

  getClientAttachements(id) {
    return axios.get(API_URL + id + '/get-attachments');
  }

  getAttachmentLink(attachmentId) {
    return (API_URL + attachmentId+ '/attachment');
  }

  dummyCreate() {
    return axios.post(API_URL + 'dummy-create', { headers: authHeader() });
  }

  uploadFile(id, fd) {
    let header = authHeader();
    header['Content-Type'] = 'multipart/form-data';
    return axios.post(API_URL + id + '/upload-file', fd, { headers: header});
  }

}

export default new ClientService();
